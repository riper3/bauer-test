# Introduction
**Read the full instructions before beginning**

We're looking for how you solve the problems faced, not complete perfection, however, we would like to see an effort made in all aspects of development.

You can take this as an opportunity to learn and push yourself to do something you've never done before, if it doesn't work out quite as you'd hoped, don't panic, just explain what you've done and what else you would've done if you had more time.

# The Files
In this folder, you'll find an illustrator design file `code-test.ai` along with a `code-test.jpg` screenshot of the design if you don't have illustrator.

- Image assets have already been extracted to `assets/images`
- Fonts can be found in `assets/fonts`

# The Tasks
## Task 1 - The Build
Build the design in HTML & CSS, ensuring the page is responsive for mobile use. Feel free to use Bootstrap or other front-end frameworks to complete your task.

## Task 2 - The Game
The game is shown in the design `code-test.ai`.

### Game functionality
A bag should *slide* across the screen from the left and stop on the platform for weighing. 

The user should be given *one* chance to guess each bag weight.

Once the user guesses, the bag should disappear.
Each correct/incorrect guess should notify the user of the result of that guess.
A new bag with a different weight should slide from the left and the form field should be emptied.


### Technical stuff
We'd like you to build this with HTML/CSS, and add the additional game functionality with JavaScript for the front-end, and any language of your choice for the back-end.

When the user guesses the weight of a bag, it should POST **using AJAX** the `id` of the bag and the `guessed_weight` to a back-end. 

The back-end should get the bag from the database using the `id` provided, and compare the `bag_weight` of the bag with the user inputted `guessed_weight`. Once the check has been done, the back-end should return back JSON, so the JavaScript can check if the result was correct/incorrect and display the correct message.

Feel free to use any JS framework or libraries to complete this, like jQuery or even React/Vue, etc.

### What constitutes a "correct" guess?
The `guessed_weight` should be correct within a 15% range of the bags weight.
Example psuedo-code
```
if( guessed_weight / bag_weight >= 0.85 && guessed_weight / bag_weight <>= 1.15) {
	// successful guess
} else {
	// unsuccessful guess
}
```


### Useful resources
https://laracasts.com/series/php-for-beginners




# Sending it back to us
When you're done, make sure things are documented so we know whats going on, and some instructions on how to set up your project.
