<?php
function CheckLuggage($luggage, $guessed_weight) {
  require "bbdd.php";

  $luggageid = explode("e", $luggage)[1];

  $sql = "SELECT weight FROM luggages WHERE id = $luggageid";

  $weight = $conn->query($sql)->fetch_assoc()['weight'];

  $conn->close();

  if($guessed_weight/$weight >= 0.85 && $guessed_weight/$weight <= 1.15) {
    $checkgussed = array("guessed" => "Correct");
  }
  else {
    $checkgussed = array("guessed" => "Incorrect");
  }

  return json_encode($checkgussed);
}

if (isset($_POST['idluggage']) && isset($_POST['guessed_weight'])) {

     echo CheckLuggage($_POST['idluggage'], $_POST['guessed_weight']);
}
