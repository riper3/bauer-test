// Slider
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
}

// Game
$("#submit").click(function(){
  if($("#weight").val())
  {
    $.ajax({
        url: 'phpfunctions/checkluggage.php',
        type: "POST",
        data: { "idluggage": $("#luggage").attr("alt"),
                "guessed_weight": $("#weight").val()},
        success: function(reponse) {
          var answer = JSON.parse(reponse);
          $("#submit").prop('disabled', true);

          if(answer.guessed == "Correct") {
            $("#win-text").show();
          }
          else {
            $("#lost-text").show();
          }

          $("#weight").val('');
          $("#luggage").fadeOut();

          $.ajax({
              url: 'phpfunctions/newluggage.php',
              type: "POST",
              data: {"newluggage": true},
              success: function(reponse) {
                var luggage = JSON.parse(reponse);

                setTimeout(function(){
                $('#luggage').fadeOut("done");
                $("#luggage").attr("alt", "Luggage"+luggage.id);
                $("#luggage").attr("src", "assets/images/"+luggage.picture);
                $("#luggage").show();

                $("#lost-text").hide();
                $("#win-text").hide();
                $("#submit").prop('disabled', false);
                }, 2000);
              }
         });
        }
    });
  }
  else {
    var backgroundcolor = $("#weight").css("background-color");
    $("#weight").css("background-color", "#ff3333");

    setTimeout(function(){
    $("#weight").css("background-color", backgroundcolor);
    }, 3000);
  }
});
