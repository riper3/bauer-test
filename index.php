<head>
  <link rel="stylesheet" type="text/css" href="assets/css/main.css">
  <link rel="shortcut icon" type="image/png" href="assets/images/tui-logo.png"/>
</head>
<body>
  <?php
  require "phpfunctions/newluggage.php";
  $luggage = NewLuggage();
  ?>
  <div id="hero-div">
    <img src="assets/images/tui-logo.png" id="tui-logo">
  </div>
  <div id="main-div">
    <img src="assets/images/palm-tree-left.png" id="tree-left">
    <img src="assets/images/palm-tree-bg.png" id="tree-bg">
    <div id="text-win">
      <img src="assets/images/brush.png">
      <span>
        Here 's your chance to win a luxury giveaway!
      </span>
    </div>
    <div id="explain-div">
      <img src="assets/images/brush.png" id="brush-explain">
      <img src="assets/images/palm-tree-right.png" id="tree-right">
      <span id="total-text">
        <p class="first-text">
          It's all about holiday
        </p>
        <p class="first-text">
          Big Feelings!
        </p>
        <p class="second-text">
          Magic radio has teamed up with TUI to win a dream holiday to a destination of your choice!
          The warmth of the sun on your skin, dipping your toes in the pool, or the smell of the sun tan lotion
          We all love those holiday Big Feelings you get when it comes to holidays! </br>
          With TUI, you can choose from a huge range of holidays to luxury destinations. How about
          heading to Jamaica with your loved one? There is plenty to do including xxx, xxx and xxx.
        </p>
      </span>
    </div>
    <div id="carrusel-div">
      <div class="mySlides fade">
         <img src="assets/images/gallery-01.jpg">
    </div>

      <div class="mySlides fade">
        <img src="assets/images/gallery-02.jpg">
      </div>

      <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
      <a class="next" onclick="plusSlides(1)">&#10095;</a>

      <img id="coconuts" src="assets/images/coconuts.png">
    </div>
    <div id="video-div">
      <img src="default/video-screenshot.png">
    </div>
  </div>
  <div id="game-div">
    <div id="game-text">
      <h2>
        Enter our competition bellow!
      </h2>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        Nunc elementum justo sed magna dictum, sit amet hendrerit augue scelerisque.
      </p>
    </div>
    <img src="assets/images/shadow.png" id="shadow">
    <img src="assets/images/comp-background.svg" id="background-game">
    <img src="assets/images/conveyor-belt.svg" id="belt">
    <img src="assets/images/scale.svg" id="scale">
    <img src="<?php echo 'assets/images/'.$luggage['picture']; ?>" id="luggage" alt="<?php echo 'Luggage'.$luggage['id']; ?>">
    <input type="number" id="weight">
    <span id="kgholder">kg</span>
    <span id="win-text">Good job!</span>
    <span id="lost-text">Try again!</span>
    <span id="guest-the">Guess the weight</span>
  </div>
  <div id="footer">
    <input type="submit" id="submit">
  </div>
</body>
<script src="assets/js/jquery-3.4.0.min.js"></script>
<script src="assets/js/fronted.js"></script>
